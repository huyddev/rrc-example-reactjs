import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import App from './App';
import Users from './Users';
import Hooks from './Hooks';
import Courses from './Courses';
import Networks from './Networks';
import Redux from './Redux';
import Redux2 from './Redux2';
import Profile from './Profile';
import CollectionsPage from './Forms/CollectionsPage';
import 'antd/dist/antd.css';
import Book from "./Book";

function Navigation() {
  return (
    <Router>
      <div>
        {/* <Header /> */}

        <Route exact path="/" component={App} />
        <Route path="/users" component={Users} />
        <Route path="/hooks" component={Hooks} />
        <Route path="/courses" component={Courses} />
        <Route path="/networks" component={Networks} />
        <Route path="/forms" component={CollectionsPage} />
        <Route path="/redux" component={Redux} />
        <Route path="/redux2" component={Redux2} />
        <Route path="/profile" component={Profile} />
        <Route path="/book" component={Book} />
      </div>
    </Router>
  );
}

export default Navigation;