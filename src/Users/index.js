import React from 'react';
import { Link } from 'react-router-dom'
import styles from './styles';
import CustomHeader from '../common/Headers'

class Users extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            user: {
                name: '',
                class: '',
                math: 0,
                angular: 0
            },
            users: [],
            users1: [1, 5, 7]
        }
    }

    // user = {};

    _onChange(event, _type) {
        let _obj = {}
        _obj[_type] = event.target.value;
        this.setState({
            user: {
                ...this.state.user,
                ..._obj
            }
        })
    }

    _newUser = () => {
        let { users, user } = this.state;
        user.avg = (parseInt(user.math) + parseInt(user.angular))/2;
        console.log("user", user)
        users.push(user);
        this.setState({ users })
    }

    _renderUsers() {
        console.log("arrow", this.state.users)
        return this.state.users.map((u, index) => {
            return <li key={index}>{u.name} - {u.class} - {u.math} - {u.angular} - {u.avg}</li>
        })
    }

    _navigateTo(_route) {
        this.props.history.push({
            pathname: _route,
            state: {
                courses: 'courses'
            }
        });
    }

    static getDerivedStateFromProps(props, state) {
        console.log("getDerivedStateFromProps", props, state)
        return null;
    }

    _showUserProfile = () => {
        alert(1);
        this.setState({
            users1: [6, 7, -1]
        }, () => {
            console.log("thay doi state users1", this.state.users1)
        })
        
    }

    render() {
        // console.log("props", this.props)
        return (
            <div>
                <CustomHeader onRightAction={this._showUserProfile} userData={this.state.users1}>List user</CustomHeader>
                <ul>
                    {this._renderUsers()}
                </ul>
                <input placeholder="enter name" onChange={(event) => this._onChange(event, 'name')} /><br />
                <input placeholder="enter class" onChange={(event) => this._onChange(event, 'class')} /><br />
                <input placeholder="enter math point" onChange={(event) => this._onChange(event, 'math')} /><br />
                <input placeholder="enter angular point" onChange={(event) => this._onChange(event, 'angular')} /><br />
                <button onClick={this._newUser}>New User</button>
                <button onClick={this._navigateTo.bind(this, '/courses')}>Navigation to hooks</button>
                <Link to="/hooks">Navigate to hooks</Link>
            </div>
        )
    }
}

export default Users;