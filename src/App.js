import React from 'react';
import logo from './logo.svg';
import './App.css';
import AppStyles from './AppStyles';
import Users from './Users';
import Hooks from './Hooks';
import Async from './Async';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      title: 'Hello Mta 11111',
      user: {
        fullname: 'nam anh',
        nhanPham: 0
      },
      one: 1
    }
  }

  globalComponent = 0;

  componentWillMount() {
    console.log("componentWillMount", this.nhanpham_namanh);
  }

  componentWillUpdate() {
    console.log("will update", this.nhanpham_namanh);
  }

  componentWillReceiveProps(nextProps, nextState) {
    console.log("will receive props", nextState);
  }

  render() {
    console.log("render", this.globalComponent);
    let namanh = 'nhan pham';
    return (
      <div className="App">
        <h1>{this.state.title}</h1>
        <h1>{namanh}</h1>
        <h1 style={{ backgroundColor: 'red' }}>Fullname: {this.state.user.fullname}</h1>
        {(this.state.one == 1 && this.state.two == 1)
          ? <div>
            <h1 style={AppStyles.textGreen}>Nhan pham: {this.state.user.nhanPham}</h1>
          </div>
          : this.state.three == 3
            ? <h1>three</h1>
            : <h1>else</h1>
        }
        {/* <Hooks /> */}
        <Users user={this.state.user} />
        <Async />
      </div>
    );
  }

  componentDidMount() {
    console.log("did mount", this.nhanpham_namanh);
    // setTimeout(() => {
    //   this.setState({
    //     title: 'Mta wellcome nam anh',
    //     user: {
    //       ...this.state.user,
    //       nhanPham: 1
    //     }
    //   })
    // }, 3000)
  }

  componentDidUpdate() {
    console.log("did update", this.nhanpham_namanh);
  }

  componentWillUnmount() {

  }

}

export default App;
