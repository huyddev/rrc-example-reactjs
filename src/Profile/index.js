import React, { Component } from 'react';
import { connect } from 'react-redux';

class Profile extends Component {
    constructor(props) {
        super(props)
    }

    navigateBook() {
        this.props.history.push('/book')
    }

    componentDidMount() {
        console.log("did mount", this.props.user)
    }

    render() {
        return (
            <div>
                profile
                <button onClick={this.navigateBook.bind(this)}>Go to Book</button>
            </div>
        );
    }
}

function bindAction(dispatch) {
    return {
        
    };
}

const mapStateToProps = state => ({
    user: state.userReducer
});

export default connect(mapStateToProps, bindAction)(Profile);