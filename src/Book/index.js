import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Input, Tooltip, Icon, Button } from 'antd';
import { _setBook } from '../Redux2/actions/bookAction';

class Book extends Component {
    constructor(props) {
        super(props)
        this.state = {
            book: ""
        }
    }

    _setValueBook(event) {
        this.setState({
            book: event.target.value
        })
    }

    saveBook() {
        let { book } = this.state;
        this.props._setBook(book);
    }

    componentDidMount() {

    }

    componentWillReceiveProps(nextProps) {
        console.log("old props", this.props.user)
        console.log("next props", nextProps.user)
    }

    render() {
        console.log("book", this.props.user)
        return (
            <div>
                <ul>
                    {this.props.book.data.map((b, index) => {
                        return <li key={index}>{b}</li>
                    })}
                </ul>
                <Input
                    onChange={this._setValueBook.bind(this)}
                    placeholder="Enter your book"
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    suffix={
                        <Tooltip title="Extra information">
                            <Icon type="info-circle" style={{ color: 'rgba(0,0,0,.45)' }} />
                        </Tooltip>
                    }
                />
                <Button type="danger" onClick={() => this.saveBook()}>Danger</Button>
            </div>
        );
    }
}

function bindAction(dispatch) {
    return {
        _setBook: (book) => dispatch(_setBook(book))
    };
}

const mapStateToProps = state => ({
    book: state.bookReducer,
    user: state.userReducer
});

export default connect(mapStateToProps, bindAction)(Book);