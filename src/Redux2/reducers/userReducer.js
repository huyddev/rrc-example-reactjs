
import { SET_USER, PRE_SET_USER } from '../actions/types'

const DEFAULT_STATE = {
    data: {},
    friends: [],
    follows: []
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case PRE_SET_USER: {
            if(state.data.username && state.data.username == 'hello redux') {
                return {
                    ...state,
                    friends: [1]
                }
            }
            return {
                ...state
            }
        }
        case SET_USER:
            return {
                ...state,
                data: action.payload
            }
        default:
            return state;
    }
}