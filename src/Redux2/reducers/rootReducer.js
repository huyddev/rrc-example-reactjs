import { combineReducers } from 'redux';
import userReducer from './userReducer';
import bookReducer from './bookReducer';
// import courseReducer from './courseReducer';
export default combineReducers({
    userReducer,
    bookReducer
    // courseReducer
});