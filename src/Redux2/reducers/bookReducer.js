
import { SET_BOOK } from '../actions/types'

const DEFAULT_STATE = {
    data: []
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case SET_BOOK: {
            let { data } = state;
            data.push(action.payload)
            return {
                ...state,
                data
            }
        }
            
        default:
            return state;
    }
}