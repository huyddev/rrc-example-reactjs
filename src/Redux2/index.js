import React from 'react';
import { connect } from 'react-redux';
import { _setUser } from './actions/userActions'

class Redux2 extends React.Component {

    constructor(props) {
        super(props)
    }

    _doLogin() {
        const user = {
            username: "hello redux",
            password: "1234567"
        };
        this.props._setUser(user);
        this.props.history.push({
            pathname: '/profile',
            state: {
                user
            }
        });
    }

    render() {
        return(
            <div>
                <button onClick={this._doLogin.bind(this)}>Login</button>
            </div>
        )
    }
}

function bindAction(dispatch) {
    return {
        _setUser: (user) => dispatch(_setUser(user)),

    };
}

const mapStateToProps = state => ({
    user: state.userReducer
});

export default connect(mapStateToProps, bindAction)(Redux2);