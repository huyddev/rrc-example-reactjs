import { createStore, applyMiddleware, compose } from 'redux';
import storage from 'redux-persist/lib/storage'
import { persistStore, persistReducer } from 'redux-persist'
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer.js';
import promise from './promise';
import devTools from 'remote-redux-devtools';

const persistConfig = {
    key: 'root',
    storage: storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export default function configureStore() {
  const enhancer = compose(
    applyMiddleware(thunk, promise),
    (devTools({
      name: 'helloReact', realtime: true,
    }))
  );

  const store = createStore(persistedReducer, enhancer);
  persistStore(
    store,
    null,
    () => {
      
    }
  )
  return store;
}