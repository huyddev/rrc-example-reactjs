import { SET_BOOK } from './types';

function handleSetBook(book) {
    return {
        type: SET_BOOK,
        payload: book
    }
}

export function _setBook(book) {
    return (dispatch) => {
        dispatch(handleSetBook(book))
    }
}