import { SET_USER, PRE_SET_USER } from './types';

function preSetUser() {
    return {
        type: PRE_SET_USER
    }
}

function handleSetUser(user) {
    return {
        type: SET_USER,
        payload: user
    }
}

export function _setUser(user) {
    return (dispatch) => {
        dispatch(preSetUser());
        dispatch(handleSetUser(user))
    }
}