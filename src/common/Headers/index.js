import React from 'react';

class CustomHeader extends React.Component {

    _doClickRight() {
        this.props.onRightAction();
    }

    componentDidMount() {
        console.log("custom header", this.props.userData)
    }

    render() {
        return <div style={{ width: 400, backgroundColor: 'pink', display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
            <div>HKKK</div>
            <div>{this.props.children}</div>
            <div>
                <button onClick={this._doClickRight.bind(this)}>User profile</button>
            </div>
        </div>
    }
}

export default CustomHeader;