import React from 'react'
import axios from 'axios';
import CustomHeader from '../common/Headers'

class Networks extends React.Component {

    constructor(props) {
        super(props)
    }

    _getUsers() {
        return fetch('https://randomuser.me/api/');
    }

    _postContact() {
        let contact = {
            email: 'test.cmc@gmail.com',
            phone: '0943345554',
            name: 'test.name',
            content: 'i like nlms'
        }
        axios.post(`http://108.160.133.232:4040/api/contacts`, contact)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
    }

    async componentDidMount() {
        let a = fetch('https://randomuser.me/api/');
        a.then((res) => res.json()).then(data => console.log("data", data))
        let b = await this._getUsers();
        let bData = await b.json()
        console.log("Networks did mount", bData);

        // this._postContact();
    }

    _showCurrentNetworks() {
        alert(2)
    }

    render() {
        return (
            <div>
                <CustomHeader onRightAction={this._showCurrentNetworks.bind(this)}>List networks</CustomHeader>
                Networks
            </div>
        )
    }
}

export default Networks;