import React from 'react'

class Async extends React.Component {

    constructor(props) {
        super(props);
    }

    _workIn3s() {
        return new Promise((resolve) => {
            setTimeout(() => {
                console.log("work 3s")
                resolve();
            }, 3000)
        })
    }

    _workIn5s() {
        return new Promise((resolve) => {
            setTimeout(() => {
                console.log("work 5s")
                resolve();
            }, 5000)
        })
    }

    async componentDidMount() {
        await this._workIn3s();
        await this._workIn5s();
    }

    render() {
        return (
            <div>

            </div>
        )
    }
}

export default Async;