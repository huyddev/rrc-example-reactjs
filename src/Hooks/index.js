import React, { useState } from 'react';
import { tsConstructorType } from '@babel/types';

export default function Hooks() {
    
    const [count, setCount] = useState(0);

    return(
        <div>
            <p>You clicked {count} times</p>
            <button onClick={() => setCount(count + 1)}>its Hooks</button>
        </div>
    )
}