
import { GET_COURSE, SET_COURSE } from '../actions/types'

const DEFAULT_STATE = {
    lists: [],
    carts: []
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case GET_COURSE:
            return {
                ...state
            }
        case SET_COURSE:
            return {
                ...state,
                lists: action.payload
            }
        default:
            return state;
    }
}