import { GET_COURSE, SET_COURSE } from './types'

export function getCourseActions() {
    return {
        type: GET_COURSE
        // payload: childId
    }
}

export function setCourseActions(listData) {
    console.log("action setCourseActions", listData)
    return {
        type: SET_COURSE,
        payload: listData
    }
}