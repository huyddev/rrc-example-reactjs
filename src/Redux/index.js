import React from 'react'
import { DatePicker } from 'antd';
import { connect } from 'react-redux';
import { getCourseActions, setCourseActions } from './actions/courseActions'

class Redux extends React.Component {

    constructor(props) {
        super(props)
    }

    _doSetCourseList() {
        // const listCourses = [
        //     1, 2, 3, 4, 5
        // ]
        // this.props.setCourseActions(listCourses);
        
    }

    _doGetCourseList() {
        console.log("get course form store", this.props.courses)
    }

    _navigateCourse() {
        this.props.history.push({
            pathname: '/courses',
            state: {
                
            }
        });
    }

    componentDidMount() {

    }

    render() {
        return (
            <div>
                <DatePicker />
                <button onClick={() => this._doSetCourseList()}>Set Courses List</button>
                <button onClick={() => this._doGetCourseList()}>Get Courses List</button>
                <button onClick={() => this._navigateCourse()}>Go Course</button>
            </div>
        )
    }
}

function bindAction(dispatch) {
    return {
        getCourseActions: () => dispatch(getCourseActions()),
        setCourseActions: (listData) => dispatch(setCourseActions(listData))
    };
}

const mapStateToProps = state => ({
    courses: state.courseReducer,
    // user: state.userReducers
});

export default connect(mapStateToProps, bindAction)(Redux);