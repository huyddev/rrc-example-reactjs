import React from 'react'
import { DatePicker } from 'antd';
import { connect } from 'react-redux';

class Courses extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        console.log("course did mount", this.props)
    }

    render() {
        return(
            <div>
                <DatePicker />
            </div>
        )
    }
}

function bindAction(dispatch) {
    return {
        
    };
}

const mapStateToProps = state => ({
    courses: state.courseReducer.lists,
    // user: state.userReducers
});

export default connect(mapStateToProps, bindAction)(Courses);